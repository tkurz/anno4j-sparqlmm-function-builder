package com.github.anno4j.extension.sparqlmm.functions;

import com.github.tkurz.sparqlmm.Constants;
import com.google.common.base.Joiner;
import org.apache.maven.plugin.logging.Log;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.openrdf.query.algebra.evaluation.function.Function;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * @author Thomas Kurz (tkurz@apache.org)
 * @since 22.10.15.
 */
public class FunctionBuilder {

    private static final String BASE_URI = Constants.NAMESPACE;

    private Set<Function> functions;

    private Log log;

    private VelocityEngine engine;

    public FunctionBuilder(Set<String> packages, Log log) {

        engine = new VelocityEngine();
        Properties properties = new Properties();
        properties.setProperty("resource.loader", "file");
        properties.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        engine.init(properties);

        this.log = log;

        this.functions = new HashSet<Function>();

        ServiceLoader<Function> loader = ServiceLoader.load(Function.class);
        //select functions
        for(Function function : loader) {
            if(function.getURI().startsWith(BASE_URI)) {
                if(packages.contains(function.getClass().getPackage().getName())) {
                    this.functions.add(function);
                }
            }
        }
    }

    public void writeClasses(Path path) throws IOException {

        Path basicTarget = path.resolve("com/github/anno4j/extension/sparqlmm");
        Files.createDirectories(basicTarget);

        Path functionTarget = path.resolve("com/github/anno4j/extension/sparqlmm/functions");
        Files.createDirectories(functionTarget);

        Path expressionTarget = path.resolve("com/github/anno4j/extension/sparqlmm/expression");
        Files.createDirectories(expressionTarget);

        ArrayList<String> function_classes = new ArrayList<>();
        ArrayList<String> import_classes = new ArrayList<>();

        for(Function function : this.functions) {
            log.info(String.format("Write class files for sparql function %s", function.getURI()));

            Context context = new VelocityContext();

            context.put("name",function.getURI().substring(Constants.NAMESPACE.length()));
            context.put("class_name", function.getClass().getSimpleName() + "Test");
            context.put("expression_name", "E_" + function.getClass().getSimpleName());
            context.put("url",function.getURI());

            writeFunctionClass(functionTarget, context);
            writeExpressionClass(expressionTarget, context);

            function_classes.add("\t"+function.getClass().getSimpleName()+".class");
            import_classes.add("import "+function.getClass().getName() + ";");
        }

        Context context = new VelocityContext();

        context.put("function_classes", Joiner.on(",\n").join(function_classes));
        context.put("import_classes", Joiner.on("\n").join(import_classes));

        final Path vFile = basicTarget.resolve("SparqlMMFunctionTest.java");
        try(PrintWriter writer = new PrintWriter(vFile.toFile())) {
            Template template = engine.getTemplate("SparqlMMFunctionTest.vm");
            template.merge(context, writer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void writeFunctionClass(Path path, Context context) {
        final Path vFile = path.resolve(context.get("class_name").toString()+".java");
        try(PrintWriter writer = new PrintWriter(vFile.toFile())) {
            Template template = engine.getTemplate("TestClassTemplate.vm");
            template.merge(context, writer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void writeExpressionClass(Path path, Context context) {
        final Path vFile = path.resolve(context.get("expression_name").toString()+".java");
        try(PrintWriter writer = new PrintWriter(vFile.toFile())) {
            Template template = engine.getTemplate("ExpressionClassTemplate.vm");
            template.merge(context, writer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
