package com.github.anno4j.extension.sparqlmm.functions;

import com.google.common.collect.ImmutableSet;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.util.Set;

/**
 * @author Thomas Kurz (tkurz@apache.org)
 * @since 22.10.15.
 */
public class FunctionBuilderTest {

    @Rule
    public TemporaryFolder folder= new TemporaryFolder();

    private Set<String> packages = ImmutableSet.of(
            "com.github.tkurz.sparqlmm.function.spatial.relation.directional"
    );

    private Log log = new SystemStreamLog();



    @Test
    public void testSimpleFunctionBuild() throws IOException {
        FunctionBuilder builder = new FunctionBuilder(packages, log);
        builder.writeClasses(folder.getRoot().toPath());

        System.out.println(folder.getRoot().getAbsolutePath());

        //TODO test
    }

}
